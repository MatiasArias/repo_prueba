# -*- coding: utf-8 -*-


{
    "name" : "registro_actividades",
    "description" : """ Módulo integral para el uso de la empresa SADE """,
    "version" : "0.1",
    "depends" : ['suri'],
    "category": "Custom",
    "author" : "Matias Exequiel Arias",
    "url": "www.sade-sa.com",
    "init_xml":[],
    "update_xml": ['vista_regAct.xml',
                   ],

    "data": [],
    "installable" : True,
    "active" : False # No se instala el módulo al crear la base de datos.
}
