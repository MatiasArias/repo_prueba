# -*- coding: utf-8 -*-
       
from osv import fields, osv
from pprint import pprint
from datetime import date, datetime
from openerp import SUPERUSER_ID


def _obtener_adjuntos(self, cr, uid, ids, field_name, arg, context):
        res = {}
        attach_pool = self.pool.get('ir.attachment')
        for i in ids:
            attach = attach_pool.search(cr, uid, [('res_id', '=', i), ('res_model', '=', self._name), ('send_mail','=',True)])
            res[i] = attach
        return res



def _agregar_adjunto_actividad(self, cr, uid, id, name, value, arg, context=None):
        """Agrega adjunto a la actividad""" 
        adjuntos_pool = self.pool.get('ir.attachment')
        for adjunto in value:
            if adjunto[0]==4: # El adjunto no se modifica ni crea
                continue
            if adjunto[0]==1: # El adjunto se modifica
                id_adjunto = adjunto[1]
                adjuntos_pool.write(cr, uid, [id_adjunto], adjunto[2])
                continue
            if adjunto[0]==0: # El adjunto se crea
                # Cargo los campos por defecto y los relaciono a la compra.
                data_adjunto = adjunto[2]
                data_adjunto['res_model'] = 'registro_actividades.actividades'
                data_adjunto['res_id'] = id
                data_adjunto['type'] = 'binary'
                data_adjunto['send_mail'] = True
                adjuntos_pool.create(cr, uid, data_adjunto, context)
                continue
        return True


def _dummy(self, cr, uid, ids, name, arg, context=None):
    """Metodo dummy para campo al que se le usa el search view"""
    return {}

def search_default_mis_actividades(self, cr, uid, obj, name, args, context):
    """Funcion que filtra una condicion en el xml"""
    now = datetime.now()
    now = now.strftime('%Y-%m-%d')
    
    act_id = self.pool.get('registro_actividades.actividades')
    res = []
    for field, operator, value in args:
            act_ids = act_id.search(cr, SUPERUSER_ID, [('dia', '=', now)])
            res.append(('id', 'in', act_ids))
    return res


def calcular_horas(self, cr, uid, ids, field_name, values, arg, context=None):
    res ={}
    now = datetime.now()
    now = now.strftime('%Y-%m-%d')
    suma = 0     
  
    #Traer todos los ids de los objetos que cumplan la condicion
    ids_actividades = self.search(cr,uid,[('dia','=',now)])
    
    #Traigo los objetos enteros y calculo la suma de horas
    for record in self.browse(cr, uid, ids_actividades, context=context):
        suma += record.horas
    
    for id in ids:
            
        res[id]= suma
            
    return res
    
    
    
class actividades(osv.osv):
    """Clase que representa el registro de actividades"""

    def onchange_horas(self, cr, uid, ids, horas):
        if (horas >= 0 and horas <= 8):
            return True
        else:
            warning = {
                'title': 'Error: Horas',
                'message' : 'la hora debe ser un valor entre 0 y 8 '
            }
            return {'warning':warning, 'value': {'horas':0}}


    _name = "registro_actividades.actividades"
    _columns = {
        'usuario': fields.many2one('res.users','Usuario', required=True),
        'proyecto': fields.many2one('suri.proyectos.proyecto','Proyecto', required=True),
        'actividad': fields.text("Actividad", required=True, help='Actividad que se registra'),
        'categoria': fields.selection([('capacitacion','Capacitacion'),('licencia','Licencia'),('actividad','Actividad'),('otro','Otros'),], 'Categoria'),
        'horas': fields.float('Horas',digits=(1,2)),
        'dia': fields.date('Dia',required=True),
        'uid_actividades': fields.function(_dummy, fnct_search=search_default_mis_actividades, type='one2many', string='Actividades'),
        'sumHoras': fields.function(calcular_horas, fnct_inv=None, method=True, type='float',string='Horas Totales'),
        'adjuntos':fields.function(_obtener_adjuntos,fnct_inv=_agregar_adjunto_actividad, method=True, type='one2many', obj='ir.attachment', string='Adjuntos a enviar'),
    }
    #Campo que posee el valor a mostrar
    _rec_name = 'usuario'


    def create(self, cr, uid, values, context=None):
    #El usuario es el que crea la actividad
        values['usuario']=uid 
        res = super(actividades, self).create(cr, uid, values, context=context)  
        return res   
    
    def write(self, cr, uid, ids, values, context=None):
    #El usuario es el que edita la actividad
        values['usuario']=uid 
        res = super(actividades, self).write(cr, uid, ids, values, context=context) 
        return res 
    
    def unlink(self, cr, uid, ids, context=None):
        res = super(actividades, self).unlink(cr, uid, ids, context=context)
        return res
        
actividades()


class herencia_usuario(osv.osv):
    """Clase que hereda res.users"""
    
    _name = 'res.users'
    _inherit = 'res.users'
    
    _columns = {
            'actividad': fields.one2many('registro_actividades.actividades', 'usuario','Registro de actividades'),
               }




herencia_usuario()

class herencia_proyecto(osv.osv):
    """Clase que hereda suri.proyectos.proyecto"""
    
    _name = 'suri.proyectos.proyecto'
    _inherit = 'suri.proyectos.proyecto'

    _columns = {
            'actividad': fields.one2many('registro_actividades.actividades', 'proyecto','Registro de actividades'),
               }



herencia_proyecto()



